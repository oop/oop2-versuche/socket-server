import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class Main {

	interface PeerListener {
		void unregister(Socket socket);

		void publish(Player player, Socket socket);
	}

	static class ClientHandler implements Runnable {

		private final PeerListener peerListener;
		private final Socket clientSocket;
		private final byte[] buffer = new byte[100000];

		public ClientHandler(Socket clientSocket, PeerListener unregisterListener) {
			this.peerListener = unregisterListener;
			this.clientSocket = clientSocket;
		}

		public void send(String data) {
			try (clientSocket) {
				clientSocket.getOutputStream().write(data.getBytes("utf-8"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			System.out.printf("Handling Cient: %s\n", clientSocket.getInetAddress().getHostAddress());
			try (clientSocket) {
				for (;;) {
					int len = clientSocket.getInputStream().read(buffer);
					if (len == 0 || len < 0) {
						break;
					}
					System.out.printf("Read %d Bytes\n", len);
					String str = new String(buffer, 0, len, "utf-8");
					System.out.println(str.trim());
					String[] parts = str.trim().split(",");
					if (parts.length < 4) {
						System.out.println("invalid message");
						continue;
					}

					try {

						int x = Integer.valueOf(parts[0]);
						int y = Integer.valueOf(parts[1]);
						int radius = Integer.valueOf(parts[2]);
						String color = parts[3];

						peerListener.publish(new Player(x, y, radius, color), clientSocket);
					} catch (Exception e) {

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				System.out.printf("Cient Hung up: %s\n", clientSocket.getInetAddress().getHostAddress());
				peerListener.unregister(clientSocket);
			}

		}

	}

	private final static Object[] lock = new Object[] {};

	public static void main(String[] args) {
		System.out.println("Socket Server");

		HashMap<Socket, Player> peers = new HashMap<Socket, Player>();

		Thread publishThread = new Thread(new Runnable() {
			@Override
			public void run() {
				for (;;) {
					synchronized (lock) {
						try {
							lock.wait();
						} catch (InterruptedException ie) {
							ie.printStackTrace();
						}

						String data = "";
						for (Player player : peers.values()) {
							if (player == null) {
								continue;
							}
							data += String.format("%s\n", player.encode());
						}

						byte[] payload;
						try {
							payload = data.getBytes("utf-8");
						} catch (UnsupportedEncodingException e1) {
							e1.printStackTrace();
							return;
						}

						for (Socket s : peers.keySet()) {
							try {
								s.getOutputStream().write(payload);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}

			}
		}, "Publish Thread");
		publishThread.start();

		try (ServerSocket sock = new ServerSocket(12221)) {
			for (;;) {
				Socket client = sock.accept();
				synchronized (lock) {
					peers.put(client, null);
				}

				ClientHandler clientHandler = new ClientHandler(client, new PeerListener() {
					@Override
					public void unregister(Socket socket) {
						synchronized (lock) {
							peers.remove(socket);
							lock.notify();
						}
					}

					@Override
					public void publish(Player player, Socket socket) {
						synchronized (lock) {
							peers.put(socket, player);
							lock.notify();
						}
					}
				});
				Thread handlerThread = new Thread(clientHandler,
						String.format("Handler Thread for %s", client.getInetAddress().getHostAddress()));

				handlerThread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

class Player {

	int x, y;
	String color;
	int radius;

	public Player(int x, int y, int radius, String color) {
		this.x = x;
		this.y = y;
		this.color = color;
		this.radius = radius;
	}

	public String encode() {
		return String.format("%d,%d,%d,%s", x, y, radius, color);
	}

}
